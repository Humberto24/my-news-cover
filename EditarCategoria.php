<?php
    require 'logica/conexion.php';
    $id = $_GET['id'];

    $consultar = "SELECT * from categorias where id = '$id'";
    $query = mysqli_query($conectar,$consultar); 
    $array = mysqli_fetch_array($query);

	session_start();
    $user = $_SESSION['user'];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/AgregarCategoria.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>
    <div class="arriba">
        <img class="logo" src="img/logo.jpg" alt="logo">
        <div class="dropdown">
            <button style="margin-left: 850px; margin-top: 20px" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $user['nombre'];?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="logica/CerrarSesion.php">Logout</a>
                <a class="dropdown-item" href="Noticia.php">News Sources</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="texto1">
            <h1 class="text">Categories</h1>
            <hr>
        </div>

        <form method="POST" name="form-work" action="logica/EditarCategoria.php?id=<?php echo $array['id']?>">
            <br><br>
            <input value="<?php echo $array['categoria']?>" type="text" name="nombre" placeholder="Name">
            <br><br><br><br>
            <br><br><br><br>
            <hr class="secondLine">
            <br>

            <button type="submit" class="button">Save</button>

            <br><br><br><br>
        </form>
    </div>

    <br><br>
    <hr>
    <div class="footer">
        <div class="fintxt">
            <a>MyCover</a>
            <a href="">|</a>
            <a>About</a>
            <a href="">|</a>
            <a>Help</a>
        </div>
    </div>
    <br>
    <p style="margin-right: 80px" class="final"><span class="logo">&copy;</span>My News Cover</p>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>