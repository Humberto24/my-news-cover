<?php

$message = "";
if(!empty($_REQUEST['status'])) {
  switch($_REQUEST['status']) {
	case 'login':
	  $message = 'User does not exists';
	break;
	case 'error':
	  $message = 'There was a problem inserting the user';
	break;
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="css/Login.css">
</head>
<body>
	<div class="arriba">
		<img class="logo" src="img/logo.jpg" alt="logo">
		<button class="botonInicial">Login</button>
	</div>
	<br><br><br>
	<div class="container">
		<div class="texto1">
			<h1 class="text">User Login</h1>
			<hr>
		</div>

	<form action="IniciarSesion.php" method="POST" class="form-inline" role="form">
	  <br><br>
      <div class="form-group">
        <input type="text" class="form-control" id="" name="username" placeholder="Email Address">
      </div>
	  <br><br>
      <div class="form-group">
        <input type="password" class="form-control" id="" name="password" placeholder="Password">
      </div>
	  <br><br>
	  <hr>
	  <br><br>
      <button type="submit" class="button">Login</button>
	  <br><br>
	  <p>If you don't have an account <a href="Registro.php">Signup here</a></p>
    </form>
	</div>
        
		<br><br><hr>
		<div class="footer">
			<div class="fintxt">
				<a>MyCover</a>
                <a href="">|</a>
				<a>About</a>
				<a href="">|</a>
				<a>Help</a>
			</div>
		</div>
		<p style="margin-right: 100px" class="final"><span class="logo">&copy;</span>My News Cover</p>
</body>
</html>