<?php
require 'logica/conexion.php';
session_start();
$user = $_SESSION['user'];


$consulta = 'SELECT * FROM categorias';
$query = mysqli_query($conectar, $consulta);
$array = mysqli_fetch_array($query);

$idUser = $user['id'];


$consulta1 = "SELECT * FROM noticias where id_usuario = '$idUser'";
$query1 = mysqli_query($conectar, $consulta1);
$array1 = mysqli_fetch_array($query1);

if($array1 == ""){
    header("Location: AgregarFuenteNoticia.php");
}



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/Principal.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>

    <div class="arriba">
        <img class="logo" src="img/logo.jpg" alt="logo">
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $user['nombre'];?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="logica/CerrarSesion.php">Logout</a>
                <a class="dropdown-item" href="Noticia.php">News Sources</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="texto1">
            <h1 class="text">Your Unique News Cover</h1>
            <hr class="first">
            <br>
        </div>

        <div class="cuadritos">
            <?php
            foreach($query as $row){
            ?>
            <div class="card" style="width: 12rem;">
                <ul class="list-group list-group-flush">
                    <form method="post" action="">
                        <input style="visibility: hidden;" type="text" name="getId" placeholder="Enter website feed URL"
                            value="<?php echo $row['id']?>">
                        <a href="logica/categoriaPorId.php"><button type="submit" name="submit" class="list-group-item"
                                name="botonCategoria"><?php echo $row['categoria']?></button></a>
                    </form>
                </ul>
            </div>
            <?php
            }
            ?>
        </div>
        <br><br>
        <?php
        if(isset($_POST['submit'])){    
            if($_POST['getId'] != ''){
                $id = $_POST['getId'];
                $consulta1 = "SELECT * FROM noticias where id_categoria = '$id' AND id_usuario = '$idUser'";
                $query1 = mysqli_query($conectar, $consulta1);
                $array1 = mysqli_fetch_array($query1);
            }
            if($_POST['getId'] == null){
                
            }
        }
        ?>
        <div class="card-columns">
            <?php
                foreach ($query1 as $row){?>
            <div class="card mb-3">
                <div class="card-body">
                    <p class="card-text"><?php echo ($row['fecha']);?></p>
                </div>
                <div class="card-body">
                    <h5 class="card-title"><?php echo ($row['titulo']);?></h5>
                    <p class="card-text"><?php echo ($row['descripcion']);?></p>
                    <h6 class="card-title"><?php 
                    foreach($query as $rowss){
                    if($row['id_categoria'] == $rowss['id']){
                    echo ($rowss['categoria']);
                        }
                    }
                    ?>

                    </h6>

                </div>
                <div class="card-footer">
                    <a href="<?php echo ($row['link']);?>" class="card-link">Ver Noticia</a>
                </div>
            </div>
            <?php
                }
                ?>
        </div>
    </div>

    <br><br>
    <hr>
    <div class="footer">
        <div class="fintxt">
            <a>MyCover</a>
            <a href="">|</a>
            <a>About</a>
            <a href="">|</a>
            <a>Help</a>
        </div>
    </div>
    <br>
    <p style="margin-right: 150px" class="final"><span class="logo">&copy;</span>My News Cover</p>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>

</body>

</html>