<?php
require 'logica/consultasCategoria.php';

session_start();
$user = $_SESSION['user'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/Categoria1.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>

    <div class="arriba" style="margin-top: 40px">
        <img class="logo" src="img/logo.jpg" alt="logo">
        <div class="dropdown">
            <button style="margin-left: 900px; margin-top: 10px" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $user['nombre'];?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="logica/CerrarSesion.php">Logout</a>
                <a class="dropdown-item" href="Noticia.php">News Sources</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="texto1">
            <h1 class="text">Categories</h1>
            <hr>
        </div>

        <div>

            <center>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="datos">
                        <?php
            foreach ($query as $row){?>
                        <tr>
                            <td><?php echo $row['categoria'];?></td>
                            <td>
                                <a href="EditarCategoria.php?id=<?php echo $row['id'] ?>"><button type="button"
                                        class="btn btn-success">Edit</button></a>
                                <a href="logica/EliminarCategoria.php?id=<?php echo $row['id'] ?>"><button type="button"
                                        class="btn btn-danger">Delete</button></a>
                            </td>
                        </tr>
                    </tbody>

                    <?php
        }
        ?>
                </table>
            </center>

            <div class="cuerpo">

                <a class="button" href="AgregarCategoria.php">Add New</a>
                <br><br><br><br><br><br>
            </div>
        </div>

        <hr>
        <div class="footer">
            <div class="fintxt">
                <a>MyCover</a>
                <a href="">|</a>
                <a>About</a>
                <a href="">|</a>
                <a>Help</a>
            </div>
        </div>
        <br>
        <p style="margin-right: 100px" class="final"><span class="logo">&copy;</span>My News Cover</p>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
        </script>
</body>

</html>